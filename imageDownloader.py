# Modified imageDownloader.py by FB36
# From http://code.activestate.com/recipes/577385-image-downloader/
# Modified by m0ngr31 to download images semi-automatically from HFBoards.com
import urllib2
import re
import sys
from os.path import basename
from urlparse import urlsplit

global urlList
global imgList
urlList = []
imgList = ['rating.gif', 'attachment.php', 'imager.php', '600.png', 'thumbup.gif', 'picture.php', 'image.php']

def downloadImages(url_orig, page_num):
    url = url_orig + "&page=" + str(page_num)
    print ("==============")
    print ("| On Page " + str(page_num) + " |")
    print ("==============")
    global urlList
    if url in urlList:
        return
    urlList.append(url)
    try:
        urlContent = urllib2.urlopen(url).read()
    except:
        return

    imgUrls = re.findall('<img .*?src="(.*?)"', urlContent)
    for imgUrl in imgUrls:
        try:
            if basename(urlsplit(imgUrl)[2]) in imgList:
                print("Skipping: " + basename(urlsplit(imgUrl)[2]))
            elif basename(urlsplit(imgUrl)[2]).find('.') == -1:
                print("Skipping: " + basename(urlsplit(imgUrl)[2]))
                imgList.append(basename(urlsplit(imgUrl)[2]))
            else:
                imgData = urllib2.urlopen(imgUrl).read()
                fileName = basename(urlsplit(imgUrl)[2])
                print('Downloading: ' + fileName)
                output = open(fileName,'wb')
                output.write(imgData)
                output.close()
                imgList.append(fileName)
        except:
            pass    

# main
if len(sys.argv) > 2:
    pages = sys.argv[2]
else:
    pages = "50"

if '.txt' in sys.argv[1]:
    f = open(sys.argv[1], 'r')
    for line in f:
        print line.rstrip()
        for x in xrange(1,51):
            downloadImages(line.rstrip(), x)
else:
    for x in xrange(1,(int(pages)+1)):
        downloadImages(sys.argv[1], x)
